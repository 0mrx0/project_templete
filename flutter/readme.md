# flutter-templete
#### 项目介绍
此项目采用dio，provider，开发，对dio和provider，以及路由进行封装，大大提升开发效率

#### 环境要求
1. jdk8、android-SDK、android-studio、flutter_windows_1.22.3

#### 初始化工作
1. 打开项目，执行pub-get下载依赖包
2. 成功启动spring-boot项目，运行app即可登录

#### 更多细节如下

示例参考：
1, 列表单选：PrintPage
2, 列表多选：VoicePage
3，列表刷新：MessageFragment
4，全局数据共享：MeFragment.LoginData
5, 局部数据共享：HomeFragment.ProviderWidget
6, webview使用：FindFragment

IOS代办：
1，百度语音合成
2，百度语音听写
3，百度地图flutter插件集成事项

Android代办：
1，pubspec.yaml中，引入的 flutter_plugin_baidu，插件里需修改自己申请的百度key，访问该地址https://gitee.com/gyy_xiaobaiyang/flutter_baidu_plugin.git
有相应的说明
2，需替换百度地图key，位置android/app/src/AndroidManifest.xml

开发tips：
0, 集成了百度地图，flutter视图下打包报错；需在android视图下gradlew assembleRelease打包
1，带标题栏的页面，直接引用ToolBar即可
2，不带标题栏的页面，顶部需适配状态栏：padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top - size_top_diff),


环境适配：flutter_windows_1.22.3\packages\flutter_tools\gradle\flutter.gradle文件中，找到getTargetPlatforms方法，修改如下：
private List<String> getTargetPlatforms() { return DEFAULT_PLATFORMS }
