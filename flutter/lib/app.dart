import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_picker/PickerLocalizationsDelegate.dart';
import 'package:flutter_ywt/config/router_manager.dart';
import 'package:flutter_ywt/config/theme/theme_app.dart';
import 'package:flutter_ywt/store/index.dart';
import 'package:oktoast/oktoast.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'config/router_manager.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return OKToast(
      //全局状态管理
      child: Store.init(MainApp()),
    );
  }
}

class MainApp extends StatefulWidget {
  @override
  MainAppState createState() => MainAppState();
}

class MainAppState extends State<MainApp> {
  @override
  void initState() {
    super.initState();
    //设置全局状态 上下文
    Store.setContext(context);
  }

  @override
  Widget build(BuildContext context) {
    print("-----MainApp-------build-----");
    //上拉 下拉文字提示
    return RefreshConfiguration(
      hideFooterWhenNotFull: true,
      headerBuilder: () => ClassicHeader(
        releaseText: '松开刷新',
        refreshingText: '正在加载...',
        failedText: '加载失败!!!',
        completeText: '加载完成',
        idleText: '下拉刷新',
      ),
      footerBuilder: () => ClassicFooter(
        noDataText: '我是有底线的~',
        loadingText: '正在加载',
        failedText: '加载失败!!!',
        idleText: '上拉加载更多',
        canLoadingText: '松开开始加载',
        // outerBuilder: (context) => Container(
        //   decoration: BoxDecoration(border: Border(bottom: BorderSide(width: 0.5, color: Color(0xff878787)))),
        // )
      ),
      child: MaterialApp(
        title: 'flutter_app',
        debugShowCheckedModeBanner: false,
        theme: AppTheme.getThemeData('default'),
        //设置路由器
        onGenerateRoute: RouteInit.generateRouter,
        initialRoute: RouteName.login,

        localizationsDelegates: [
          PickerLocalizationsDelegate.delegate, // 如果要使用本地化，请添加此行，则可以显示中文按钮
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en', 'US'),
          const Locale('zh', 'CH'),
          const Locale('ko', 'KO'),
          const Locale('it', 'IT'),
          const Locale('ar', 'AR'),
          const Locale('tr','TR')
        ],
      ),
    );

  }
}
