import 'dart:io';

import 'package:flutter_bmfbase/BaiduMap/bmfmap_base.dart'
    show BMFMapSDK, BMF_COORD_TYPE;
import 'package:flutter_ywt/config/environment_manager.dart';
import 'package:flutter_ywt/config/ui_adapte_manager.dart';

import 'app.dart';

///生产环境 入口
void main() async {
  //初始化环境变量
  Environment.initPro();

  if(Platform.isIOS){
    //百度地图-ios，在这里设置key
    BMFMapSDK.setApiKeyAndCoordType(
        '请输入百度开放平台申请的iOS端API KEY', BMF_COORD_TYPE.BD09LL);
  }else if(Platform.isAndroid){
    //百度地图-android，在安卓项目设置key
    BMFMapSDK.setCoordType(BMF_COORD_TYPE.BD09LL);
  }

  //尺寸适配，设置 SCREEN_WIDTH = 360
  var widgetsBinding = InnerWidgetsFlutterBinding.ensureInitialized();
  widgetsBinding
    ..attachRootWidget(MyApp())
    ..scheduleForcedFrame();

  // runApp(MyApp());
}