import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ywt/constant/intent_key.dart';
import 'package:flutter_ywt/http/http_url_h5.dart';
import 'package:flutter_ywt/third/baidumap/flutter_location_page.dart';
import 'package:flutter_ywt/third/baidumap/flutter_map_page.dart';
import 'package:flutter_ywt/third/baiduvoice/baidu_voice_page.dart';
import 'package:flutter_ywt/third/picker/data_picker.dart';
import 'package:flutter_ywt/third/picker/image_picker.dart';
import 'package:flutter_ywt/third/scan/scan_page.dart';
import 'package:flutter_ywt/third/webview/flutter_webview.dart';
import 'package:flutter_ywt/third/webview/flutter_webview_find.dart';
import 'package:flutter_ywt/ui/pages/home/main_activity.dart';
import 'package:flutter_ywt/ui/pages/login/login_page.dart';
import 'package:flutter_ywt/ui/pages/me/print_page.dart';
import 'package:flutter_ywt/ui/pages/me/setting_page.dart';
import 'package:flutter_ywt/ui/pages/me/voice_page.dart';
import 'package:flutter_ywt/ui/pages/splash/splash.dart';
import 'package:flutter_ywt/ui/pages/test/test_page.dart';
import 'package:flutter_ywt/ui/weight/anim_page_route.dart';
import 'package:flutter_ywt/ui/weight/popup/test_popup_page.dart';

import '../ui/pages/more/more_feagment.dart';

///路由管理，路由跳转，路由返回
class Routers{

  static BuildContext _context;
  //放在第一个界面，初始化 _context(目前在登录页面 进行初始化)
  static setContext(BuildContext context) {
    _context ??= context;
    return context;
  }

  static push(String name, [Object arguments]) {
    return Navigator.pushNamed(_context, name, arguments: arguments);
  }

  static pop([Object arguments]) {
    Navigator.pop(_context, arguments);
  }
}

class RouteName {
  static const String scan_page = 'scan_page';
  static const String more_page = 'more_page';
  static const String flutter_webview = 'flutter_webview';
  static const String login = 'login';
  static const String splash = 'splash';
  //主界面
  static const String main = 'main';
  static const String speak_page = 'speak_page';
  //我的界面
  static const String print_page = 'print_page';
  static const String voice_page = 'voice_page';
  static const String setting_page = 'setting_page';
  //test
  static const String test = 'test';
  static const String test_popup = 'test_popup';
  static const String test_location = 'test_location';
  static const String test_map = 'test_map';
  static const String test_image = 'test_image';
  static const String test_picker = 'test_picker';
  static const String test_web_view = 'test_web_view';
}

class RouteInit {
  static Route<dynamic> generateRouter(RouteSettings settings) {
    switch (settings.name) {
      case RouteName.scan_page:
        return NoAnimRouteBuilder(FullScreenScannerPage());
      case RouteName.more_page:
        return NoAnimRouteBuilder(MoreFragment());
      case RouteName.flutter_webview:
        return NoAnimRouteBuilder(FLutterWebView(settings.arguments));
      case RouteName.login:
        return NoAnimRouteBuilder(LoginPage());
      case RouteName.splash:
        return NoAnimRouteBuilder(SplashPage());
        //主界面
      case RouteName.main:
        return NoAnimRouteBuilder(MainActivity());
      case RouteName.speak_page:
        return NoAnimRouteBuilder(BaiDuVoicePage());
        //我的界面
      case RouteName.print_page:
        return NoAnimRouteBuilder(PrintPage());
      case RouteName.voice_page:
        return NoAnimRouteBuilder(VoicePage());
      case RouteName.setting_page:
        return NoAnimRouteBuilder(SettingPage());
        //test
      case RouteName.test:
        return NoAnimRouteBuilder(TestPage());
      case RouteName.test_popup:
        return NoAnimRouteBuilder(PopupRoutePage());
      case RouteName.test_location:
        return NoAnimRouteBuilder(BaiDuLocationPage());
      case RouteName.test_map:
        return NoAnimRouteBuilder(FlutterBMFMapDemo());
      case RouteName.test_image:
        return NoAnimRouteBuilder(ImagePickerPage());
      case RouteName.test_picker:
        return NoAnimRouteBuilder(PickerDataPage());
      case RouteName.test_web_view:
        return NoAnimRouteBuilder( FindWebView({INTENT_URL: jsApiTestH5}));

      default:
        return CupertinoPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No Route Defined for ${settings.name}'),
                  ),
                ));
    }
  }
}
