///@date  :2020/11/10/16:37
///desc： 环境变量管理
///author：gyy

class Environment{
  static EnvironmentEnum enumValue;
  //网络请求的域名前缀
  static String baseUrl;
  //vue-h5项目的域名前缀
  static String h5Url;
  //该环境的tag
  static String tag;

  //初始化测试环境
  static initTest() {
    enumValue = EnvironmentEnum.test;
    baseUrl = "http://192.168.201.72:8090/";
    h5Url = "https://ywt.yimidida.com/";
    tag = "测试环境";
  }

  //初始化生产环境
  static initPro() {
    enumValue = EnvironmentEnum.pro;
    baseUrl = "http://47.101.170.41:8090/";
    h5Url = "https://ywt.yimidida.com/";
    tag = "正式环境";
  }
}

enum EnvironmentEnum{
  pro, //生产
  test //测试
}
