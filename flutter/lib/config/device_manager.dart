import 'package:device_info/device_info.dart';

///设备管理，获取设备信息
class DeviceMan {
//  DeviceMan._();
//  static final _instance = DeviceMan._();
//  factory DeviceMan.getInstance() => _instance;

  static AndroidDeviceInfo _androidInfo;
  static Future<AndroidDeviceInfo> getDeviceInfo() async {
    if(_androidInfo == null) {
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      _androidInfo = await deviceInfo.androidInfo;
      return Future.value(_androidInfo);
    }
    return Future.value(_androidInfo);
  }




}