import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

///全局的路径管理
class StorageMan {
  /// app全局配置 sp
  static SharedPreferences _sharedPreferences;

  static Future<SharedPreferences> get getSpManager async {
    if (_sharedPreferences == null) {
      _sharedPreferences = await SharedPreferences.getInstance();
    }
    return _sharedPreferences;
  }

  /// 下载路径，调用前 需申请权限（Permission.storage）
  static String _downPath;

  static Future<String> getDownPath() async {
    List<Directory> appDocDir =
        await getExternalStorageDirectories(type: StorageDirectory.downloads);
    if (appDocDir.isNotEmpty) {
      _downPath = appDocDir[0].path + "/";
    } else {
      _downPath = "/storage/emulated/0/";
    }
    return _downPath;
  }
}
