import 'package:event_bus/event_bus.dart';

///事件管理，事件统一定义在这里
//Bus初始化
EventBus eventBus = EventBus();

//进度框 进度事件
class ProgressEvent{
  double value;
  ProgressEvent(this.value);
}

//主界面，第一次返回按钮事件
class HomeBackEvent{
  HomeBackEvent();
}