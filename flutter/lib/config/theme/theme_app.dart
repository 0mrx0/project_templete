import 'package:flutter/material.dart';

import 'color.dart' show Color0, Color1;
import 'size.dart';

///app的全局样式
class AppTheme {

  static getThemeData(String theme) {
    //可以切换别的主题
    Color0.primaryColor = Color1.primaryColor;
    Color0.primaryColorDark = Color1.primaryColorDark;
    Color0.cardColor = Color1.cardColor;
    Color0.accentColor = Color1.accentColor;
    Color0.dividerColor = Color1.dividerColor;

    Color0.hintColor = Color1.hintColor;

    Color0.backgroundColor = Color1.backgroundColor;
    Color0.scaffoldBackgroundColor = Color1.scaffoldBackgroundColor;
    Color0.disabledColor = Color1.disabledColor;
    Color0.body1 = Color1.body1;
    Color0.body2 = Color1.body2;

    ThemeData themData = ThemeData(
      //主色，决定导航栏颜色
      primaryColor: Color0.primaryColor,
      primaryColorDark: Color0.primaryColorDark,
      //次级色，决定大多数Widget的颜色，如进度条、开关等。
      accentColor: Color0.accentColor,
      //卡片背景色
      cardColor: Color0.cardColor,
      //提示文字颜色
      hintColor: Color0.hintColor,
      //光标颜色
      cursorColor: Color0.hintColor,
      //分割线颜色
      dividerColor: Color0.dividerColor,
      //对话框背景色
      dialogBackgroundColor: Color0.backgroundColor,
      //scaffold 背景色
      scaffoldBackgroundColor: Color0.scaffoldBackgroundColor,
      //icon默认样式
      iconTheme: IconThemeData(size: size20),
      //按钮样式
      buttonTheme: ButtonThemeData(
          buttonColor: Color0.accentColor,
          disabledColor: Color0.disabledColor ,
          shape: StadiumBorder()),
      textTheme: TextTheme(
        // 默认文字style
        body1: TextStyle(color: Color0.body1, fontSize: body),
        body2: TextStyle(color: Color0.body2, fontSize: body),
        //下面的颜色，可以根据ui设置
        display1: TextStyle(color: Color0.body1, fontSize: display1),
        display2: TextStyle(color: Color0.body1, fontSize: display2),
        display3: TextStyle(color: Color0.body1, fontSize: display3),
        display4: TextStyle(color: Color0.body1, fontSize: display4),
      ),

    );
    return themData;
  }
}
