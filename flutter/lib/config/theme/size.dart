///@date  :2020/11/10/16:37
///desc：尺寸定义在这里，统一管理
///author：gyy

//全局尺寸适配，基准宽
const double size360= 360;
//标题栏高度适配，相减的值
const double size_top_diff= 3;
//文字尺寸
const double sub2 = 8;
const double sub1 = 11;
const double body = 14;
const double display1 = 17;
const double display2 = 20;
const double display3 = 23;
const double display4 = 26;

const double size200 = 200;
const double size160 = 160;
const double size150 = 150;
const double size140 = 140;
const double size120 = 120;
const double size110 = 110;
const double size100 = 100;
const double size90 = 90;
const double size80 = 80;
const double size70 = 70;
const double size60 = 60;
const double size50 = 50;
const double size40 = 40;
const double size35= 35;
const double size30 = 30;
const double size25 = 25;
const double size20 = 20;
const double size15 = 15;
const double size10 = 10;
const double size9 = 9;
const double size8 = 8;
const double size7 = 7;
const double size6 = 6;
const double size5 = 5;
const double size4 = 4;
const double size3 = 3;
const double size2 = 2;
const double size1 = 1;

