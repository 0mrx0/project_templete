import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
//主题色, 用颜色的时候，用这个
class Color0  {
  static Color primaryColor;
  static Color primaryColorDark;
  static Color  accentColor;
  static Color cardColor;
  static Color  hintColor;
  static Color  dividerColor;
  static Color  backgroundColor;
  static Color  scaffoldBackgroundColor;
  static Color  body1;
  static Color  body2;
  static Color disabledColor;
}

//不要在控件上用
class Color1 {
  static Color primaryColor  = Colors.white;
  static Color primaryColorDark= Color(0xdddddddd);
  static Color  accentColor= Colors.amber;
  static Color cardColor= Colors.white;
  static Color  hintColor= Colors.amber;
  static Color  dividerColor= Color(0xff878787);
  static Color  backgroundColor= primaryColor;
  static Color  scaffoldBackgroundColor= Color(0xfff5f5f5);
  static Color  body1=Colors.black87;
  static Color  body2= Colors.white;
  static Color disabledColor= Color(0xff878787);
}

//不要在控件上用
class Color2  {
  static Color primaryColor  = Colors.red;
  static Color primaryColorDark= Colors.red;
  static Color  accentColor= Colors.red;
  static Color cardColor= Colors.red;
  static Color  hintColor= Colors.red;
  static Color  dividerColor=Colors.red;
  static Color  dialogBackgroundColor= Colors.red;
  static Color  scaffoldBackgroundColor= Colors.red;
  static Color  body1=Colors.red;
  static Color  body2= Colors.red;
  static Color disabledColor= Colors.red;
}
