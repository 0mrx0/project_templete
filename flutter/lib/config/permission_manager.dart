import 'package:permission_handler/permission_handler.dart';
///权限管理，申请权限
class PermissionMan{

  ///存储权限
  static Future<bool> storagePermission() async {
    final status = await Permission.storage.request();
    if (status == PermissionStatus.granted) {
      return true;
    } else {
      return false;
    }
  }

  ///位置权限
  static Future<bool> locationPermission() async {
    final status = await Permission.location.request();
    if (status == PermissionStatus.granted) {
      return true;
    } else {
      return false;
    }
  }

  ///麦克风
  static Future<bool> speechPermission() async {
    final status = await Permission.speech.request();
    if (status == PermissionStatus.granted) {
      return true;
    } else {
      return false;
    }
  }

  ///phone
  static Future<bool> phonePermission() async {
    final status = await Permission.phone.request();
    if (status == PermissionStatus.granted) {
      return true;
    } else {
      return false;
    }
  }

  ///百度语音
  static Future<bool> baiDuPermission() async {
    bool storage = await storagePermission();
    if(!storage) {
      return Future.value(false);
    }
    bool speech = await speechPermission();
    if(!speech) {
      return Future.value(false);
    }
    bool phone = await phonePermission();
    if(!phone) {
      return Future.value(false);
    }
    return Future.value(true);
  }


}