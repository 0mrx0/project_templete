///打印机设备model
class PrintDeviceBean{
  String name;
  String address;
  String type;
  bool check;

  PrintDeviceBean({this.name, this.address, this.type, this.check = false});

  factory PrintDeviceBean.fromJson(Map<dynamic, dynamic> json) {
    return PrintDeviceBean(
      name: json["name"],
      address: json["address"],
      type: json["type"],
      check: json["check"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "name": this.name,
      "address": this.address,
      "type": this.type,
      "check": this.check,
    };
  }
}