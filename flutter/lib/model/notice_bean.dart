import 'package:dio/src/dio_error.dart';

import 'base/response_error.dart';

class NoticeData implements ResponseError{
  int id;
  String createTime;
  String updateTime;
  String title;
  String content;
  int status;
  int userId;
  String readTime;
  bool isRead;

  NoticeData(
      {this.id,
        this.createTime,
        this.updateTime,
        this.title,
        this.content,
        this.status,
        this.userId,
        this.readTime,
        this.isRead});

  NoticeData.fromJson(Map<dynamic, dynamic> json) {
    dioError = json['dioError'];

    id = json['id'];
    createTime = json['createTime'];
    updateTime = json['updateTime'];
    title = json['title'];
    content = json['content'];
    status = json['status'];
    userId = json['userId'];
    readTime = json['readTime'];
    isRead = json['isRead'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['createTime'] = this.createTime;
    data['updateTime'] = this.updateTime;
    data['title'] = this.title;
    data['content'] = this.content;
    data['status'] = this.status;
    data['userId'] = this.userId;
    data['readTime'] = this.readTime;
    data['isRead'] = this.isRead;
    return data;
  }

  @override
  DioError dioError;
}
