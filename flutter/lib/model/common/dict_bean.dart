import 'package:dio/src/dio_error.dart';

import '../base/response_error.dart';

///数据字典的实体
class DictBean implements ResponseError{
  int id;
  String createTime;
  String updateTime;
  String type;
  String k;
  String val;

  DictBean(
      {this.id, this.createTime, this.updateTime, this.type, this.k, this.val});

  DictBean.fromJson(Map<dynamic, dynamic> json) {
    dioError = json['dioError'];

    id = json['id'];
    createTime = json['createTime'];
    updateTime = json['updateTime'];
    type = json['type'];
    k = json['k'];
    val = json['val'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['createTime'] = this.createTime;
    data['updateTime'] = this.updateTime;
    data['type'] = this.type;
    data['k'] = this.k;
    data['val'] = this.val;
    return data;
  }

  @override
  DioError dioError;
}
