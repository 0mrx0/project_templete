///权限实体
class PermissionMenu {
  int createTime;
  String css;
  String href;
  int id;
  String name;
  int parentId;
  String permission;
  int sort;
  int type;
  int updateTime;

  PermissionMenu(
      {this.createTime,
        this.css,
        this.href,
        this.id,
        this.name,
        this.parentId,
        this.permission,
        this.sort,
        this.type,
        this.updateTime});

  PermissionMenu.fromJson(Map<String, dynamic> json) {
    createTime = json['createTime'];
    css = json['css'];
    href = json['href'];
    id = json['id'];
    name = json['name'];
    parentId = json['parentId'];
    permission = json['permission'];
    sort = json['sort'];
    type = json['type'];
    updateTime = json['updateTime'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createTime'] = this.createTime;
    data['css'] = this.css;
    data['href'] = this.href;
    data['id'] = this.id;
    data['name'] = this.name;
    data['parentId'] = this.parentId;
    data['permission'] = this.permission;
    data['sort'] = this.sort;
    data['type'] = this.type;
    data['updateTime'] = this.updateTime;
    return data;
  }
}