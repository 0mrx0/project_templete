
import 'package:dio/src/dio_error.dart';

import '../base/response_error.dart';
import '../common/permission.dart';

///登录成功 返回的实体

/// 1,实现 ResponseError
/// 2，fromJson方法中 添加 dioError = json['dioError'];
/// 3，fromJson(Map<String, dynamic> json) 改为 fromJson(Map<dynamic, dynamic> json)
class LoginData implements ResponseError{
  bool enabled;
  int expireTime;
  int loginTime;
  //用户昵称
  String nickname;
  //密码 加密的
  String password;
  //权限集合
  List<PermissionMenu> permissions;
  String phone;
  int sex;
  int status;
  String telephone;
  String token;
  int updateTime;
  //登录用户名
  String username;
  //h5需要的token字段
  String CASTGC;

  LoginData(
      {this.enabled,
        this.expireTime,
        this.loginTime,
        this.nickname,
        this.password,
        this.permissions,
        this.phone,
        this.sex,
        this.status,
        this.telephone,
        this.token,
        this.updateTime,
        this.username});

  LoginData.fromJson(Map<dynamic, dynamic> json) {
    dioError = json['dioError'];

    enabled = json['enabled'];
    expireTime = json['expireTime'];
    loginTime = json['loginTime'];
    nickname = json['nickname'];
    password = json['password'];
    if (json['permissions'] != null) {
      permissions = new List<PermissionMenu>();
      json['permissions'].forEach((v) {
        permissions.add(new PermissionMenu.fromJson(v));
      });
    }
    phone = json['phone'];
    sex = json['sex'];
    status = json['status'];
    telephone = json['telephone'];
    token = json['token'];
    updateTime = json['updateTime'];
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['enabled'] = this.enabled;
    data['expireTime'] = this.expireTime;
    data['loginTime'] = this.loginTime;
    data['nickname'] = this.nickname;
    data['password'] = this.password;
    if (this.permissions != null) {
      data['permissions'] = this.permissions.map((v) => v.toJson()).toList();
    }
    data['phone'] = this.phone;
    data['sex'] = this.sex;
    data['status'] = this.status;
    data['telephone'] = this.telephone;
    data['token'] = this.token;
    data['updateTime'] = this.updateTime;
    data['username'] = this.username;
    data['CASTGC'] = this.token;
    return data;
  }

  @override
  DioError dioError;
}
