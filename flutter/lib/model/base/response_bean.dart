import 'package:dio/dio.dart';
import 'package:flutter_ywt/model/base/response_error.dart';

///@date  :2020/11/10/16:37
///desc： 不需要返回数据的，可以用这个做响应实体
///author：gyy

class ResponseBean implements ResponseError{
  dynamic data;
  bool success;


  ResponseBean({this.data, this.success});

  ResponseBean.fromJson(Map<String, dynamic> json) {
    dioError = json['dioError'];
    data = json['data'];
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.data;
    data['success'] = this.success;
    return data;
  }

  @override
  DioError dioError;
}
