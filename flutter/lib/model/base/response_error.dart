import 'package:dio/dio.dart';

///@date  :2020/11/10/16:37
///desc： 所有的网络请求实体，都实现该接口，赋值 dioError（见LoginData类中的备注）
///author：gyy

abstract class ResponseError {
  DioError dioError;

  ResponseError({this.dioError});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dioError'] = this.dioError;
    return data;
  }
}
