import 'package:flutter_ywt/model/common/dict_bean.dart';

import 'http_api.dart';
import 'http_url.dart';

///接口管理类 - 通用模块
class CommonRepository {

  //请求数据字典，根据id
  static Future dictPostById(Map<String, dynamic> queryParameters) async {
    var resp = await yhGet(dict_id, data: queryParameters);
    return DictBean.fromJson(resp.data  ?? {});
  }

}
