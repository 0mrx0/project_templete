import 'package:flutter_ywt/model/login/login_data.dart';

import 'http_api.dart';
import 'http_url.dart';

///接口管理类 - 登录模块
class LoginRepository {

  //登录
  static Future loginPost(Map<String, dynamic> queryParameters) async {
    var resp = await yhPost(login, data: queryParameters);
    return LoginData.fromJson(resp.data  ?? {});
  }

}
