import 'package:flutter_ywt/config/environment_manager.dart';
final h5Url = Environment.h5Url;

///h5请求的接口，前缀是h5Url

//首页-老板
final home_main= h5Url + "home/main";
//首页-司机
final home_operator = h5Url + "home/operator";
//首页-客服
final home_customerService = h5Url + "home/customerService";
//派送签收
final deliversH5 = h5Url + "delivers";
//测试页
final jsApiTestH5 = h5Url + "jsApiTest";
//发现页
final findH5 = h5Url + "find";
//统计
final tongjiH5= "dataStatistics";