import 'package:flutter_ywt/model/notice_bean.dart';
import 'http_api.dart';
import 'http_url.dart';

///接口管理类, 主页
class HomeRepository {

  //获取公告 - 消息页面
  static Future publishedPost(Map<String, dynamic> queryParameters) async {
    var resp = await yhPost(published_list, data: queryParameters);
    return resp.data.map((item) => NoticeData.fromJson(item)).toList();
  }


}
