import 'package:flutter/material.dart';

enum RightType {
  NONE, //此类型会隐藏widget
  TEXT, //
  ICON, //
}

///appBar
class ToolBar extends StatelessWidget implements PreferredSizeWidget {
  final int maxHeight;
  final String title;
  final String extra;
  final Color backgroundColor;
  final Color childColor;
  final IconData leftIcon;
  final IconData rightIcon;
  final bool visibilityLeft;
  final bool visibilityCenter;
  final RightType rightType;
  final VoidCallback onBackPressed;
  final VoidCallback onExtraPressed;

  static Color backgroundDefaultColor = Colors.black;
  static Color childDefaultColor = Color(0xFFFFFFFF);
  static const IconData leftIconDefault = Icons.keyboard_arrow_left;

  ///
  ///
  ///
  ///
  ToolBar(
      {this.maxHeight,
      this.title,
      this.extra,
      this.backgroundColor,
      this.childColor,
      this.leftIcon = leftIconDefault,
      this.rightIcon,
      this.visibilityLeft = true,
      this.visibilityCenter = true,
      this.rightType = RightType.NONE,
      this.onBackPressed,
      this.onExtraPressed});

  @override
  Widget build(BuildContext context) {
    backgroundDefaultColor = Theme.of(context).primaryColor;
    childDefaultColor = Theme.of(context).accentColor;
    return title != null ? Container(
        color: backgroundColor ?? backgroundDefaultColor,
        //适配安全区域（状态栏）
        padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).padding.top, 0, 3),
        height: 70,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Visibility(
              visible: visibilityLeft,
              maintainSize: true,
              maintainAnimation: true,
              maintainState: true,
              child: GestureDetector(
                child: Container(
                  padding: EdgeInsets.fromLTRB(6, 0, 6, 0),
                  child: Icon(
                    leftIcon,
                    color: childColor ?? childDefaultColor,
                  ),
                ),
                onTap: onBackPressed ?? () => Navigator.pop(context),
              ),
            ),
            Visibility(
              visible: visibilityCenter,
              maintainSize: true,
              maintainAnimation: true,
              maintainState: true,
              child: Text(
                title ?? 'App',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 18,
                    color: childColor ?? childDefaultColor,
                    fontWeight: FontWeight.w600),
              ),
            ),
            Stack(
              children: <Widget>[
                Visibility(
                  visible: rightType == RightType.TEXT,
                  maintainSize: true,
                  maintainAnimation: true,
                  maintainState: true,
                  child: GestureDetector(
                    child: Container(
                      alignment: Alignment.centerRight,
                      padding: EdgeInsets.fromLTRB(6, 0, 6, 0),
                      child: Text(
                        extra ?? 'text',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16,
                            color: childColor ?? childDefaultColor),
                      ),
                    ),
                    onTap: onExtraPressed,
                  ),
                ),
                Visibility(
                    maintainSize: true,
                    maintainAnimation: true,
                    maintainState: true,
                    visible: rightType == RightType.ICON,
                    //true隐藏
                    child: GestureDetector(
                      child: Container(
                        padding: EdgeInsets.fromLTRB(6, 0, 6, 0),
                        alignment: Alignment.center,
                        child: Icon(
                          rightIcon ?? Icons.close,
                          color: childColor ?? childDefaultColor,
                        ),
                      ),
                    ))
              ],
            )
          ],
        )
    )
        : SizedBox();
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
