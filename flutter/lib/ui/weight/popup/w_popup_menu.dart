import 'package:flutter/material.dart';
import 'package:flutter_ywt/config/event_manager.dart';

import 'triangle_painter.dart';

const double _kMenuScreenPadding = 8.0;

///https://github.com/fluttercandies/w_popup_menu.git

class PopupMenu extends StatefulWidget{
  PopupMenu({
    Key key,
    @required this.onValueChanged,
    @required this.actions,
    @required this.child,
    this.pressType = PressType.singleClick,
    this.backgroundColor = Colors.white,
    this.menuWidth = 180,
    this.menuHeight = 220,
  });

  final ValueChanged<int> onValueChanged;
  final List<String> actions;
  final Widget child;
  final PressType pressType; // 点击方式 长按 还是单击
  final Color backgroundColor;
  final double menuWidth;
  final double menuHeight;


  @override
  _WPopupMenuState createState() => _WPopupMenuState();
}
OverlayEntry entry;

class _WPopupMenuState extends State<PopupMenu> {
  double width;
  double height;
  RenderBox button;
  RenderBox overlay;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((call) {
      width = context.size.width;
      height = context.size.height;
      button = context.findRenderObject();
      overlay = Overlay.of(context).context.findRenderObject();
    });
    eventBus.on<HomeBackEvent>().listen((event) {
      removeOverlay();
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        removeOverlay();
        return Future.value(true);
      },
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        child: widget.child,
        onTap: () {
          if (widget.pressType == PressType.singleClick) {
            onTap();
          }
        },
        onLongPress: () {
          if (widget.pressType == PressType.longPress) {
            onTap();
          }
        },
      ),
    );
  }

  void onTap() {
    Widget menuWidget =  _MenuPopWidget(
      context,
      height,
      width,
      widget.actions,
      widget.backgroundColor,
      widget.menuWidth,
      widget.menuHeight,
      button,
      overlay,
          (index) {
        if (index != -1) widget.onValueChanged(index);
        removeOverlay();
      },
    );

    entry = OverlayEntry(builder: (context) {
      return menuWidget;
    });
    Overlay.of(context).insert(entry);
  }


}

void removeOverlay() {
  if(entry != null) {
    entry.remove();
    entry = null;
  }
}

enum PressType {
  // 长按
  longPress,
  // 单击
  singleClick,
}

class _MenuPopWidget extends StatefulWidget {
  final BuildContext btnContext;
  final List<String> actions;
  final Color backgroundColor;
  final double menuWidth;
  final double menuHeight;
  final double _height;
  final double _width;
  final RenderBox button;
  final RenderBox overlay;
  final ValueChanged<int> onValueChanged;

  _MenuPopWidget(
      this.btnContext,
      this._height,
      this._width,
      this.actions,
      this.backgroundColor,
      this.menuWidth,
      this.menuHeight,
      this.button,
      this.overlay,
      this.onValueChanged,
      );

  @override
  _MenuPopWidgetState createState() => _MenuPopWidgetState();
}

class _MenuPopWidgetState extends State<_MenuPopWidget> {

  final double _triangleHeight = 10;
  RelativeRect position;

  @override
  void initState() {
    super.initState();
    position = RelativeRect.fromRect(
      Rect.fromPoints(
        widget.button.localToGlobal(Offset.zero, ancestor: widget.overlay),
        widget.button.localToGlobal(Offset.zero, ancestor: widget.overlay),
      ),

      Offset.zero & widget.overlay.size,
    );
  }

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      //点击外部，消息弹出框
      onTap: (){
        widget.onValueChanged(-1);
      },
      child: MediaQuery.removePadding(
        context: context,
        removeTop: true,
        removeBottom: true,
        removeLeft: true,
        removeRight: true,
        child: Builder(
          builder: (BuildContext context) {
            var isInverted = (position.top +
                (MediaQuery.of(context).size.height -
                    position.top -
                    position.bottom) /
                    2.0 -
                (widget.menuHeight + _triangleHeight)) <
                (widget.menuHeight + _triangleHeight) * 2;

            return CustomSingleChildLayout(
              // 这里计算偏移量
              delegate: _PopupMenuRouteLayout(
                  position,
                  widget.menuHeight + _triangleHeight,
                  Directionality.of(widget.btnContext),
                  widget._width,
                  widget.menuWidth,
                  widget._height),
              child: SizedBox(
                height: widget.menuHeight + _triangleHeight,
                width: widget.menuWidth,
                child: Material(
                  color: Colors.transparent,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      isInverted
                          ? CustomPaint(
                        size: Size(widget.menuWidth, _triangleHeight),
                        painter: TrianglePainter(
                          color: widget.backgroundColor,
                          position: position,
                          isInverted: true,
                          size: widget.button.size,
                          screenWidth: MediaQuery.of(context).size.width,
                        ),
                      )
                          : Container(),

                        Container(
                          decoration: BoxDecoration(
                            boxShadow: const [
                              BoxShadow(blurRadius: 6.0, offset: Offset(3,3), color: Colors.black54),
                            ],),
                          height: widget.menuHeight,
                          child: _buildList(widget.actions),
                        ),

                      isInverted
                          ? Container()
                          : CustomPaint(
                        size: Size(widget.menuWidth, _triangleHeight),
                        painter: TrianglePainter(
                          color: widget.backgroundColor,
                          position: position,
                          size: widget.button.size,
                          screenWidth: MediaQuery.of(context).size.width,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),

      ),
    );
  }

  Widget _buildList(List<String> actions) {
    return ListView.separated(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: actions.length,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          onTap: () {
            widget.onValueChanged( index);
          },
          child: SizedBox(
            width:widget.menuWidth,
            height: widget.menuHeight /actions.length ,
            child: Container(
              color: Colors.white,
              width: widget.menuWidth,
              alignment: Alignment.center,
              child: Text(
                widget.actions[ index]
              ),
            ),
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return Divider(
          height: 1,
        );
      },
    );
  }
}

// Positioning of the menu on the screen.
class _PopupMenuRouteLayout extends SingleChildLayoutDelegate {
  _PopupMenuRouteLayout(this.position, this.selectedItemOffset,
      this.textDirection, this.width, this.menuWidth, this.height);

  // Rectangle of underlying button, relative to the overlay's dimensions.
  final RelativeRect position;

  // The distance from the top of the menu to the middle of selected item.
  //
  // This will be null if there's no item to position in this way.
  final double selectedItemOffset;

  // Whether to prefer going to the left or to the right.
  final TextDirection textDirection;

  final double width;
  final double height;
  final double menuWidth;

  // We put the child wherever position specifies, so long as it will fit within
  // the specified parent size padded (inset) by 8. If necessary, we adjust the
  // child's position so that it fits.

  @override
  BoxConstraints getConstraintsForChild(BoxConstraints constraints) {
    // The menu can be at most the size of the overlay minus 8.0 pixels in each
    // direction.
    return BoxConstraints.loose(constraints.biggest -
        const Offset(_kMenuScreenPadding * 2.0, _kMenuScreenPadding * 2.0));
  }

  @override
  Offset getPositionForChild(Size size, Size childSize) {
    // print(size.height.toString() +" = " + size.width.toString());
    // print(childSize.height.toString() +" = " + childSize.width.toString());
    // Find the ideal vertical position.
    double y;
    if (selectedItemOffset == null) {
      y = position.top;
    } else {
      y = position.top +
          (size.height - position.top - position.bottom) / 2.0 -
          selectedItemOffset;
    }

    // Find the ideal horizontal position.
    double x;

    // 如果menu 的宽度 小于 child 的宽度，则直接把menu 放在 child 中间
    if (childSize.width < width) {
      x = position.left + (width - childSize.width) / 2;
    } else {
      // 如果靠右
      if (position.left > size.width - (position.left + width)) {
        if (size.width - (position.left + width) >
            childSize.width / 2 + _kMenuScreenPadding) {
          x = position.left - (childSize.width - width) / 2;
        } else {
          x = position.left + width - childSize.width;
        }
      } else if (position.left < size.width - (position.left + width)) {
        if (position.left > childSize.width / 2 + _kMenuScreenPadding) {
          x = position.left - (childSize.width - width) / 2;
        } else
          x = position.left;
      } else {
        x = position.right - width / 2 - childSize.width / 2;
      }
    }

    if (y < _kMenuScreenPadding)
      y = _kMenuScreenPadding;
    else if (y + childSize.height > size.height - _kMenuScreenPadding)
      y = size.height - childSize.height;
    else if (y < childSize.height * 2) {
      y = position.top + height;
    }

    // print(y.toString() + " - " + x.toString());
    return Offset(5, 70);
  }

  @override
  bool shouldRelayout(_PopupMenuRouteLayout oldDelegate) {
    return position != oldDelegate.position;
  }
}
