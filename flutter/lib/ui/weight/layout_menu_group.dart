
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ywt/common/menu_click.dart';
import 'package:flutter_ywt/config/theme/color.dart';
import 'package:flutter_ywt/config/theme/size.dart';
import 'package:flutter_ywt/model/common/permission.dart';

import '../../config/resource_mananger.dart';
//主页菜单布局
Widget menuGroup (BuildContext context, List<PermissionMenu> list,  String listTitle) {
  return Column(
      children:[
        //分类的 标题头
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(left: size20),
          height: size40,
          width: double.infinity,
          color: Color0.primaryColorDark,
          child: Text(listTitle),
        ),
        //分类的 子项
        GridView.count(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
            crossAxisCount: 4,
          children: _buildItem(list),
        ),

      ]
  );
}

///构建item
List<Widget> _buildItem(List<PermissionMenu> list) {
  return list.map((menu) => InkWell(
    onTap: ()=> MenuClick.onMenuClick(menu),
    child: buildMenuItem(menu),
  )).toList();
}

Widget buildMenuItem(PermissionMenu menu){
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Image.asset(
        ImageHelper.wrapAssets("ic_logo.png"),
        width: size25,
        height: size25,
      ),

//          menu.icnoUrl == null
//              ? Image.asset(
//                  ImageHelper.wrapAssets("ic_home_more.png"),
//                  width: size25,
//                  height: size25,
//                )
//              : Image.network(
//                  ImageHelper.wrapUrl(menu.icnoUrl),
//                  width: size25,
//                  height: size25,
//                ),
      SizedBox(
        height: size5,
      ),
      Text(menu.name)
    ],
  );
}

