import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_ywt/config/resource_mananger.dart';
import 'package:flutter_ywt/ui/weight/tool_bar.dart';


///发现页面
class FindFragment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _KnowledgePageState();
  }
}

class _KnowledgePageState extends State<FindFragment>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  //图片地址集合
  List<String> _images = new List();

  @override
  void initState() {
    super.initState();
    _images.add(
        "https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=361678185,3951463233&fm=26&gp=0.jpg");
    _images.add(
        "https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=3081029122,472813298&fm=26&gp=0.jpg");
    _images.add(
        "https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=1609525934,2067257331&fm=26&gp=0.jpg");

  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: ToolBar(
        title: "发现",
        visibilityLeft: false,
      ),
      body: ListView(
        children: <Widget>[
          Container(
            height: 170,
            child: Swiper(
              itemBuilder: (BuildContext context, int index) {
                var image = _images[index];
                return new Image.network(
                  image,
                  fit: BoxFit.fill,
                );
              },
              itemCount: _images.length,
              autoplay: true,
              // duration: 3,
              //指示器
              pagination: new SwiperPagination(),
            ),
          ),


          Container(
            margin: EdgeInsets.all(10),
            child: GridView.count(
              crossAxisCount: 3,
              shrinkWrap: true,
              crossAxisSpacing: 10,
              children: <Widget>[
                _buildItem("ic_find_weijingpin.png", "违禁品查询"),
                _buildItem("ic_find_manage_file.png", "管理制度"),
                _buildItem("ic_find_gonggao.png", "集团公告"),
                _buildItem("ic_find_bangzu.png", "使用帮助"),
                _buildItem("ic_find_wdztc.png", "总裁热线"),
                _buildItem("ic_find_yixuetang.png", "易学堂"),
              ],
            ),
          ),
        ],
      ),
    );
  }

  ///构建item
  Widget _buildItem(String image, String title) {
    return Column(
      children: <Widget>[
        Image.asset(
          ImageHelper.wrapAssets(image),
          width: 30,
          height: 30,
        ),
        SizedBox(
          height: 10,
        ),
        Text(title)
      ],
    );
  }
}
