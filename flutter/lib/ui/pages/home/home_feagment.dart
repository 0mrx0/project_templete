import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_ywt/common/menu_click.dart';
import 'package:flutter_ywt/config/router_manager.dart';
import 'package:flutter_ywt/config/theme/size.dart';
import 'package:flutter_ywt/constant/intent_key.dart';
import 'package:flutter_ywt/http/http_url_h5.dart';
import 'package:flutter_ywt/model/common/permission.dart';
import 'package:flutter_ywt/store/index.dart';
import 'package:flutter_ywt/store/store_menu.dart';
import 'package:flutter_ywt/third/webview/flutter_webview_home.dart';
import 'package:flutter_ywt/ui/weight/icon_text.dart';
import 'package:flutter_ywt/ui/weight/popup/w_popup_menu.dart';
import 'package:flutter_ywt/utils/toast_util.dart';

import '../../../config/resource_mananger.dart';
import '../../weight/layout_menu_group.dart';

final List<String> actions = [
  '淘宝',
  '微信',
  '拼多多',
  '首页',
];

final List<String> urls = [
  home_main,
  home_operator,
  home_customerService,
];

///home界面
class HomeFragment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomeFragment>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  bool showH5 = false; //是否显示h5 webview
  int index = 3; //默认的首页角标
  HomeWebView webView = HomeWebView({INTENT_URL: home_main});

  //顶部菜单
  List<PermissionMenu> headerMenu;

  //中间菜单
  List<PermissionMenu> contentMenu;

  @override
  initState() {
    super.initState();
    headerMenu = Store.value<StoreMenu>().homeHeaderMenu();
    contentMenu = Store.value<StoreMenu>().homeContentMenu();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top - size_top_diff),
        child: Stack(children: <Widget>[
          Container(
            child: Image.asset("assets/images/ic_home_background.png"),
          ),
          Column(
            children: [
              _buildHeader(context),
              Flexible(
                child: Stack(
                  children: [
                    Visibility(
                        visible: showH5,
                        child: Container(
                          child: webView,
                        )),
                    Visibility(
                        visible: !showH5,
                        child: Column(
                          children: [
                            Container(
                              width: double.infinity,
                              child: _buildHeaderCard(context, headerMenu),
                            ),
                            //公告
                            _buildNotify(),
                            //添加一点间距
                            SizedBox(height: size10),
                            Flexible(
                              flex: 1,
                              child: GridView.builder(
                                  padding: EdgeInsets.zero,
                                  itemCount: contentMenu.length,
                                  shrinkWrap: true,
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 4,
                                    crossAxisSpacing: 6,
                                    mainAxisSpacing: 6,
                                  ),
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                        PermissionMenu data = contentMenu[index];
                                    return _buildGridItem(data);
                                  }),
                            ),
                          ],
                        )),
                  ],
                ),
              ),
            ],
          ),
        ]));
  }

  Widget _buildHeader(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, size6, 0, size6),
      child: Row(
        children: <Widget>[
          PopupMenu(
            onValueChanged: (int value) {
              if (value < urls.length) {
                setState(() {
                  index = value;
                  showH5 = true;
                });
                webView.setUrl(urls[value]);
              } else {
                setState(() {
                  index = value;
                  showH5 = false;
                });
              }
            },
            actions: actions,
            child: Padding(
              padding: EdgeInsets.all(size8),
              child: IconText(
                actions[index],
                textAlign: TextAlign.left,
                icon: Icon(
                  Icons.keyboard_arrow_down,
                  color: Colors.white,
                ),
                style: TextStyle(color: Colors.amber, fontSize: body),
              ),
            ),
          ),
          Expanded(
              child: Container(
            height: size35,
            padding: EdgeInsets.only(left: size10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(size30),
                color: Colors.white),
            child: InkWell(
              onTap: () => toastUtil("点击了 查运单"),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.search,
                    color: Colors.grey,
                  ),
                  Text("查运单"),
                ],
              ),
            ),
          )),
          Padding(
            padding: EdgeInsets.all(size8),
            child: InkWell(
              onTap: () {
                Future<dynamic> speak = Routers.push(RouteName.speak_page);
                speak.then((value) => {toastUtil(value)});
              },
              child: Icon(
                Icons.keyboard_voice,
                color: Colors.amber,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildHeaderCard(BuildContext context, List<PermissionMenu> headerMenu) {
    return Wrap(
      alignment: WrapAlignment.spaceAround,
      children: <Widget>[
        _buildCardItem(
            "0",
            "已处理0",
            headerMenu
                .firstWhere((element) => element.name == "淘宝订单")),
        _buildCardItem(
            "0",
            "已处理0",
            headerMenu
                .firstWhere((element) => element.name == "微信订单")),
        _buildCardItem(
            "0",
            "已处理0",
            headerMenu
                .firstWhere((element) => element.name == "拼多多订单")),
      ],
    );
  }

  Widget _buildCardItem(String todo, String done, PermissionMenu menu) {
    return InkWell(
      onTap: () => MenuClick.onMenuClick(menu),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(size5),
          color: Colors.white,
        ),
        width: size110,
        height: size150,
        child: Stack(
          children: <Widget>[
            Align(
              child: Text(todo, style: Theme.of(context).textTheme.display3),
              alignment: Alignment(0, -0.7),
            ),
            Align(
              child: Text(menu.name,
                  style: Theme.of(context).textTheme.display1),
              alignment: Alignment(0, 0),
            ),
            Align(
              child: Text(done),
              alignment: Alignment(0, 0.8),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildNotify() {
    return Container(
        height: size25,
        padding: EdgeInsets.only(left: size6),
        margin: EdgeInsets.all(size8),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(size25), color: Colors.white),
        child: Row(
          children: <Widget>[
            Icon(
              Icons.search,
              color: Colors.grey,
            ),
            Text("新功能，新公告"),
          ],
        ));
  }

  ///构建item
  Widget _buildGridItem(PermissionMenu menu) {
    return InkWell(
      onTap: () => MenuClick.onMenuClick(menu),
      child: buildMenuItem(menu),
    );
  }
}
