import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_ywt/config/theme/size.dart';
import 'package:flutter_ywt/provider/provider_widget.dart';
import 'package:flutter_ywt/provider/view_state_widget.dart';
import 'package:flutter_ywt/provider_model/notice_model.dart';
import 'package:flutter_ywt/ui/weight/tool_bar.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../model/notice_bean.dart';

///公告界面
class MessageFragment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _StorePageState();
  }
}

class _StorePageState extends State<MessageFragment>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    //请求参数
    Map<String, dynamic> queryParameters = Map();
    queryParameters['limit'] = 10;
    queryParameters['offset'] = 0;

    return Scaffold(
      appBar: ToolBar(
        title: "公告",
        visibilityLeft: false,
      ),
      body: Container(
        margin: EdgeInsets.all(3),
        child: ProviderWidget<NoticeModel>(
          model: NoticeModel()..setParameters(queryParameters),
          onModelReady: (model) => model.initData(),
          builder: (BuildContext context, NoticeModel model, Widget child) {
            if (model.isBusy) {
              return ViewStateBusyWidget();
            }
            return SmartRefresher(
              controller: model.refreshController,
              onRefresh: model.refresh,
              onLoading: model.loadMore,
              enablePullUp: true,
              enablePullDown: true,
              child: ListView.builder(
                  itemCount: model.list.length,
                  itemBuilder: (ctx, index) {
                    NoticeData data = model.list[index];
                    return _buildItem(data);
                  }),
            );
          },
        ),
      ),
    );
  }

  Widget _buildItem(NoticeData data) {
    return Card(
      margin: EdgeInsets.all(3),
      elevation: 2,
      child: Container(
        padding: EdgeInsets.all(size6),
        child:  Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            //第一行
            Row(
              children: <Widget>[
                Expanded (
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("标题：" + data.title),
                      data.content !=null ? Text(data.content, softWrap: true) : SizedBox()
                    ],
                  ),
                ),

//              data.androidImageUrl != null
//                  ? Image.network(data.androidImageUrl, width: 60, height: 100,)
//                  : SizedBox()
              ],
            ),
            //第2行
            Text(data.createTime),
          ],
        ),
      ),
     
    );

  }
}
