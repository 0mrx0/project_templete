import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ywt/config/event_manager.dart';
import 'package:flutter_ywt/config/resource_mananger.dart';
import 'package:flutter_ywt/config/router_manager.dart';
import 'package:flutter_ywt/config/theme/size.dart';
import 'package:flutter_ywt/constant/intent_key.dart';
import 'package:flutter_ywt/http/http_url_h5.dart';
import 'package:flutter_ywt/third/webview/flutter_webview_find.dart';
import 'package:flutter_ywt/utils/toast_util.dart';
import 'package:flutter_ywt/utils/toolbar_utils.dart';
import 'package:logger/logger.dart';

import '../home/home_feagment.dart';
import '../home/me_fragment.dart';
import '../home/message_fragment.dart';
import 'find_fragment.dart';

///主界面
class MainActivity extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _MainTabState();
  }
}

class _MainTabState extends State<MainActivity> {
  var _pageController = PageController();
  int _currentIndex = 0;
  int _lastExitAt = 0;

  @override
  void initState() {
    super.initState();
    //天气预报
//    baiDuSpeechWrap();
    //状态栏设置
    ToolbarUtils.setBarStatus(false, color: Colors.black);
  }

  @override
  Widget build(BuildContext context) {
    var logger = Logger();
    Brightness brightness = Theme.of(context).brightness;
    bool isDark = Brightness.dark == brightness;
    logger.d('------------$isDark');

    return
        Scaffold(
          floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
          floatingActionButton: Container(
            margin: EdgeInsets.only(top: size20),
            width: size50,
            height: size50,
            child: InkWell(
              child: Image.asset(ImageHelper.wrapAssets("ic_home_scan.png")),
              onTap: scanBar,
            ),

          ),

          body: WillPopScope(
            onWillPop: _exit,
            child: PageView.builder(
              itemBuilder: (ctx, index) => pages[index],
              itemCount: pages.length,
              controller: _pageController,
              physics: NeverScrollableScrollPhysics(),
              // onPageChanged: (index) {
              //   setState(() {
              //     _currentIndex = index;
              //   });
              // },
            ),
          ),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            backgroundColor: Theme.of(context).cardColor,
            //选中的icon颜色
            selectedIconTheme: IconThemeData(color: Theme.of(context).accentColor),
            selectedItemColor: Theme.of(context).accentColor,
            //未选中的icon颜色
            unselectedIconTheme:
            IconThemeData(color: Theme.of(context).disabledColor),
            unselectedItemColor: Theme.of(context).disabledColor,
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.home,
                  ),
                  label: "首页"),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.message,
                  ),
                  label: "公告"),
              BottomNavigationBarItem(
                  icon: Icon(
                      null
                  ),
                  label: ""),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.find_in_page,
                  ),
                  label: "发现"),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.person,
                  ),
                  label: "我的"),
            ],
            currentIndex: _currentIndex,
            onTap: (index) {
              print("index= " + index.toString());
              if(index != 2) {
                setState(() {
                  _currentIndex = index;
                });
                _pageController.jumpToPage(index);
              }
            },
          ),
        );

  }

  Future<bool> _exit() {
    int currentAt = new DateTime.now().millisecondsSinceEpoch;
    if (currentAt - _lastExitAt > 1500) {
      toastUtil('再次点击退出APP');
      _lastExitAt = currentAt;
      //发送返回事件
      eventBus.fire(HomeBackEvent());

    } else {
      Navigator.of(context).pop();
      // AppManager.pop();
    }
    return new Future.value(false);
  }

  List<Widget> pages = <Widget>[
    HomeFragment(),
    MessageFragment(),
    null,
    FindFragment(),
    MeFragment()
  ];

  void scanBar() {
    Future res = Routers.push(RouteName.scan_page);
    res.then((msg) => toastUtil(msg.toString()));
  }
}
