import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ywt/config/resource_mananger.dart';
import 'package:flutter_ywt/config/router_manager.dart';
import 'package:flutter_ywt/config/theme/size.dart';
import 'package:flutter_ywt/model/login/login_data.dart';
import 'package:flutter_ywt/store/index.dart';
import 'package:flutter_ywt/store/store_login.dart';

///我的界面
class MeFragment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MinePageState();
  }
}

class _MinePageState extends State<MeFragment>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  LoginData data;
  @override
  initState() {
    super.initState();
    data = Store.value<StoreLogin>().mData;
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
      body: ListView(
        children: <Widget>[
          //顶部-个人信息
          Container(
            height: size150,
            color: Colors.white,
            child: Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(size20, size40, size40, 0),
                  child:  Row(
                    children: <Widget>[
                      Text(data.username, style: TextStyle(fontSize: size25, fontWeight: FontWeight.bold),),
                      SizedBox(width: size20,),
                      Text(data.nickname)
                    ],
                  ),
                ),

                Container(
                  margin: EdgeInsets.fromLTRB(size20, size80, 0, 0),
                  child:  Row(
                    children: <Widget>[
                      Text(data.username),
                      SizedBox(width: 5,),
                      Text(data.phone)
                    ],
                  ),
                ),
                Positioned(child: Image.asset(ImageHelper.wrapAssets("sunny.png"), width: size100, height: size100,),
                  bottom: 0,
                  right: 0,
                )

              ],

            ),
          ),
          //设置项
          InkWell(
            onTap: () {
              Routers.push(RouteName.print_page);
            },
            child: _settingItem("ic_my_print_manage.png", "我的打印机"),
          ),
          InkWell(
            onTap: () {
              Routers.push(RouteName.voice_page);
            },
            child: _settingItem("ic_my_voice_notify.png", "语音管理"),
          ),
          InkWell(
            onTap: () {
              _openModalBottomSheet(context);
            },
            child:_settingItem("ic_my_version.png", "推荐给好友"),
          ),
          InkWell(
            onTap: () {
              Routers.push(RouteName.setting_page);
            },
            child: _settingItem("ic_my_setting.png", "设置"),
          ),

        ],
      )
    );
  }
}

_settingItem(String image, String text) {
  return Container(
    margin: EdgeInsets.fromLTRB(12, 12, 12, 0),
    padding: EdgeInsets.all(5),
    color: Colors.white,
    height: 40,
    child: Row(
      children: <Widget>[
        Image.asset(ImageHelper.wrapAssets(image), width: 22,height: 22,),
        SizedBox(width: 20),
        Text(text)
      ],
    ),
  );
}

Future _openModalBottomSheet(BuildContext context) async {
  final option = await showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Container(
height: size140,
          margin: EdgeInsets.all(size5),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text("分享到"),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  InkWell(
                    child: Column(
                      children: [
                        Image.asset(ImageHelper.wrapAssets("ic_wechat.png") , width: 40, height: 40,),
                        SizedBox(height: 5,),
                        Text('微信',textAlign: TextAlign.center)
                      ],
                    ),
                    onTap: () {
                      Navigator.pop(context, '微信');
                    },
                  ),
                  InkWell(
                    child: Column(
                      children: [
                        Image.asset(ImageHelper.wrapAssets("ic_work_wx.png") , width: 40, height: 40,),
                        SizedBox(height: 5,),
                        Text('企业微信',textAlign: TextAlign.center)
                      ],
                    ),
                    onTap: () {
                      Navigator.pop(context, '企业微信');
                    },
                  ),
                  InkWell(
                    child: Column(
                      children: [
                        Image.asset(ImageHelper.wrapAssets("ic_share_link.png") , width: 40, height: 40,),
                        SizedBox(height: 5,),
                        Text('复制连接',textAlign: TextAlign.center)
                      ],
                    ),
                    onTap: () {
                      Navigator.pop(context, '复制连接');
                    },
                  ),
                  InkWell(
                    child: Column(
                      children: [
                        Image.asset(ImageHelper.wrapAssets("ic_share_image.png") , width: 40, height: 40,),
                        SizedBox(height: 5,),
                        Text('生成图片',textAlign: TextAlign.center)
                      ],
                    ),
                    onTap: () {
                      Navigator.pop(context, '生成图片');
                    },
                  ),

                ],
              ),
            ],
          ),

        );
      }
  );

  print(option);
}
