
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_ywt/model/common/permission.dart';
import 'package:flutter_ywt/store/index.dart';
import 'package:flutter_ywt/store/store_menu.dart';
import 'package:flutter_ywt/ui/weight/layout_menu_group.dart';
import 'package:flutter_ywt/ui/weight/tool_bar.dart';

///更多界面
class MoreFragment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<MoreFragment>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  //显示布局
  List<PermissionMenu> level2;
  List<PermissionMenu> level1;

  List<List<PermissionMenu>> list = List();
  List<String> listTitle = List();
  @override
  initState() {
    super.initState();
    level2 = Store.value<StoreMenu>().levelMenu(2);
    level1 = Store.value<StoreMenu>().levelMenu(1);
    level1.forEach((element1) {
      //每一个分类，初始化一个集合
      List<PermissionMenu> leveList = new List();
      level2.forEach((element2) {
        if(element1.id == element2.parentId) {
          leveList.add(element2);
        }
      });
      list.add(leveList);
      listTitle.add(element1.name);
    });

  }


  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: ToolBar(
        title: "任务台",
      ),
      body: SafeArea(
        child: ListView.builder(
          itemCount: list.length,
            itemBuilder: (context, index){
          return menuGroup(context, list[index], listTitle[index]);
        }),
      ),
    );
  }





}
