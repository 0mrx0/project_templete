import 'package:flutter/material.dart';

///描述：测试页面
///author：gyy

///测试文字换行
class TestPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text("测试页面"),
      ),
      body: Column(
        children: <Widget>[
          RaisedButton(
            child: Text("联系人"),
              onPressed: () async{


          }),
          _buildItem1(context),
          Divider(height: 15),
          _buildItem2(context),
          Divider(),
          _buildItem3(context),
          Divider(),
        ],
      )
    );
  }


  Widget _buildItem1(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("第一行", style: TextStyle(color:Theme.of(context).primaryColorDark)),
        Text("第二行 data.detailcrossAxisAlignmentcrossAxisAlignmentcrossAxisAlignmentcrossAxisAlignmentcross"
            "AxisAlignmentcrossAxisAlignmentcrossAxisAlignmentcrossAxisAlignment第二行",
            style: TextStyle(color:Theme.of(context).accentColor)
        ),
        Text("第三行 "),
      ],
    );
  }
  Widget _buildItem2(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("第一行", style: TextStyle(color:Theme.of(context).primaryColorDark)),
                Text("第二行 data.detailcrossAxisAlignmentcrossAxisAlignmentcrossAxisAlignmentcrossAxisAlignmentcross"
                    "AxisAlignmentcrossAxisAlignmentcrossAxisAlignmentcrossAxisAlignment第二行",
                    style: TextStyle(color:Theme.of(context).accentColor)
                )],
            ),
            Image.network("https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=344621555,2304727406&fm=26&gp=0.jpg", width: 60, height: 100,)
            // ),
          ],
        ),
        //第2行
        Text("第三行 "),
      ],
    );
  }

  Widget _buildItem3(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("第一行", style: TextStyle(color:Theme.of(context).primaryColorDark)),
                Text("第二行 data.detailcrossAxisAlignmentcrossAxisAlignmentcrossAxisAlignmentcrossAxisAlignmentcross"
                    "AxisAlignmentcrossAxisAlignmentcrossAxisAlignmentcrossAxisAlignment第二行",
                    style: TextStyle(color:Theme.of(context).accentColor)
                )],
            ),),
            Image.network("https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=344621555,2304727406&fm=26&gp=0.jpg", width: 60, height: 100,)
            // ),
          ],
        ),
        //第2行
        Text("第三行 "),
      ],
    );
  }

}