import 'package:flutter/material.dart';
import 'package:flutter_ywt/config/theme/size.dart';
import 'package:flutter_ywt/ui/weight/tool_bar.dart';

///语音管理 界面


class VoicePage extends StatefulWidget {

  @override
  _VoicePageState createState() => _VoicePageState();
}

class _VoicePageState extends State<VoicePage> {

  List<String> actions = [
    "订单",
    "工单",
    "问题件",
    "定则信息",
    "奖惩信息",
    "查天气",
    "打卡提醒",
  ];
  List<bool> switchs = [
    true,
    true,
    true,
    true,
    true,
    true,
    false,
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ToolBar(
        title: "语音管理",
      ),
      body: Container(
        margin: EdgeInsets.only(left: size5),
        child: Column(children: [
          ListView.builder(
              shrinkWrap: true,
              itemCount: actions.length,
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    Row(
                      children: [
                        Text(actions[index]),
                        Expanded(
                          flex: 1,
                          child: SizedBox(),
                        ),
                        Switch(value: switchs[index], onChanged: (val) {

                          setState(() {
                            switchs[index] = val;
                          });
                        })
                      ],
                    ),
                    Divider()
                  ],
                );
              })
        ]),
      ),
    );
  }
}

