import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_ywt/config/theme/color.dart';
import 'package:flutter_ywt/config/theme/size.dart';
import 'package:flutter_ywt/model/print_device_bean.dart';
import 'package:flutter_ywt/ui/weight/tool_bar.dart';

///打印机界面 (列表单选)

class PrintPage extends StatefulWidget {
  @override
  _PrintPage1State createState() => _PrintPage1State();
}

class _PrintPage1State extends State<PrintPage> {
  //默认不选中
  int _checkIndex = -1;

  //打印机设备列表
  List<PrintDeviceBean> printList = List();

  @override
  initState() {
    super.initState();
    printList
      ..add(PrintDeviceBean(name: "蓝牙打印机"))
      ..add(PrintDeviceBean(name: "热敏打印机"))
      ..add(PrintDeviceBean(name: "WIFI打印机"));
  }

  onCheck(int index) {
    this._checkIndex = index;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ToolBar(
        title: "我的打印机",
        extra: "操作手册",
        rightType: RightType.TEXT,
      ),
      body: ListView.builder(
        shrinkWrap: true,
        itemCount: printList.length,
        itemBuilder: (context, index) {
          return CardItem(index, _checkIndex, printList[index].name, onCheck);
        },
      ),
    );
  }
}

class CardItem extends StatelessWidget {
  int _index;
  int _checkIndex;
  String _model;
  ValueChanged<int> _callBack;

  CardItem(this._index, this._checkIndex, this._model, this._callBack);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        height: size110,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
              left: size10,
              top: 0,
              bottom: 0,
              child: Icon(Icons.print_outlined),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(_model),
                RaisedButton(
                    color: _index == _checkIndex
                        ? Color0.accentColor
                        : Color0.scaffoldBackgroundColor,
                    child: _index == _checkIndex ? Text("已连接") : Text("点击连接"),
                    onPressed: () {
                      _callBack(_index);
                    })
              ],
            ),
          ],
        ),
      ),
    );
  }
}
