import 'package:flutter_ywt/constant/menu_data.dart';
import 'package:flutter_ywt/model/common/permission.dart';
import 'package:flutter_ywt/third/urllauncher/url_launcher.dart';
import 'package:flutter_ywt/utils/toast_util.dart';

import '../config/router_manager.dart';

///处理菜单点击事件
class MenuClick {
  static onMenuClick(PermissionMenu data) {
    if(data == null) {
      toastUtil("菜单为空，不可跳转");
      return;
    }

    //不是h5界面，我们就跳转路由
    String name = data.name;
    switch (name) {
      case APP_MENU_SCAN: //扫描
        Routers.push(RouteName.scan_page);
        return;
      case APP_MENU_IMAGE: //相册
        Routers.push(RouteName.test_image);
        return;
      case APP_MENU_MAP: //地图
        Routers.push(RouteName.test_map);
        return;
      case APP_MENU_VOICE: //语音
        Routers.push(RouteName.speak_page);
        return;
      case APP_MENU_PICKER: //选择器
        Routers.push(RouteName.test_picker);
        return;
      case APP_MENU_CALL: //打电话
        UrlLauncher.tel("15238374545");
        return;
      case APP_MENU_WEB: //测试webview
        Routers.push(RouteName.test_web_view);
        return;
      case "更多": //更多
        Routers.push(RouteName.more_page);
        return;
    }
    toastUtil(data.name);
  }
}
