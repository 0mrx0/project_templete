
//顶部3个菜单
const List headerList = ["微信订单", "淘宝订单","拼多多订单"];

//中间菜单
const List contentList = ["扫描", "相册","地图","语音", "选择器","打电话","webview"];

const String APP_MENU = "移动菜单(APP端使用)";
const String APP_MENU_WECHAT = "微信订单";
const String APP_MENU_TAOBAO = "淘宝订单";
const String APP_MENU_PINDD = "拼多多订单";

const String APP_MENU_SCAN = "扫描";
const String APP_MENU_IMAGE = "相册";
const String APP_MENU_MAP = "地图";
const String APP_MENU_VOICE = "语音";
const String APP_MENU_PICKER = "选择器";
const String APP_MENU_CALL = "打电话";
const String APP_MENU_WEB = "webview";

