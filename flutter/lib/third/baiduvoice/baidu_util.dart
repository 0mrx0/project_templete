
import 'package:flutter/services.dart';
import 'package:flutter_plugin_baidu/flutter_plugin_baidu.dart';
import 'package:flutter_ywt/config/permission_manager.dart';

import 'baidu_ars.dart';
///语音听写
Future<String> baiDuArsStartWrap() async{
  bool permissionOk = await PermissionMan.baiDuPermission();
  if(!permissionOk) {
    return "-100";
  }
  return await baiDuArsStart();
}

///语音播报
Future<void> baiDuSpeechWrap() async {
  String platformVersion;
  try {
    platformVersion = await FlutterPluginBaidu.speech("主人好，今天天气怎么样啊");
  } on PlatformException {
    platformVersion = 'Failed to get platform version.';
  }
}