import 'package:flutter/services.dart';
import 'package:flutter_plugin_baidu/flutter_plugin_baidu.dart';

///@date  :2020/11/10/16:37
///desc：测试页面
///author：gyy

Future<String> baiDuArsStart() async {
  return await FlutterPluginBaidu.start();
}

Future<void> baiDuArsCancel() async {
  String platformVersion;
  try {
    platformVersion = await FlutterPluginBaidu.cancel();
  } on PlatformException {
    platformVersion = 'Failed to get platform version.';
  }
}

Future<void> baiDuArsStop() async {
  String platformVersion;
  try {
    platformVersion = await FlutterPluginBaidu.stop();
  } on PlatformException {
    platformVersion = 'Failed to get platform version.';
  }
}
