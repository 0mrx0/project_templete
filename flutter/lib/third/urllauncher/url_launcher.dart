import 'package:url_launcher/url_launcher.dart';
class UrlLauncher{
  //打电话
  static tel(String phone) async {
    final url = 'tel:$phone';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }


}