import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_ywt/ui/weight/tool_bar.dart';
import 'package:image_picker/image_picker.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: ImagePickerPage(),
    );
  }
}

class ImagePickerPage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<ImagePickerPage> {
  File _image;
  final picker = ImagePicker();

  Future getImage(ImageSource source) async {
    final pickedFile = await picker.getImage(source: source);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ToolBar(
        title:"Image Picker ",
      ),
      body: ListView(
        children: [
          Row(
            children: [
              RaisedButton(
                child: Text("相机"),
                  onPressed: ()=> getImage(ImageSource.camera)),
              RaisedButton(
                  child: Text("相册"),
                  onPressed: ()=> getImage(ImageSource.gallery)),
            ],
          ),
          _image == null
              ? Text('No image selected.')
              : Image.file(_image),
        ],

      )
    );
  }
}