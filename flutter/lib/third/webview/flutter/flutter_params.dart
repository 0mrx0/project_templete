
class FlutterParams {
    // 1-成功，0-失败
     int code = 1;
     //额外的信息
     String message;
     //返回的数据
     Object data;
     //回调的js方法
     String jsCallBackId;

     Map<String, dynamic> toJson() {
         final Map<String, dynamic> data = new Map<String, dynamic>();
         data['code'] = this.code;
         data['message'] = this.message;
         data['data'] = this.data;
         data['jsCallBackId'] = this.jsCallBackId;
         return data;
     }
}
