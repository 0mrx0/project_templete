
/**
 * 通讯录获取到的 联系人的姓名 电话
 */
 class FlutterContact {
     String name;
     String phone;

     Map<String, dynamic> toJson() {
    return {
      "name": this.name,
      "phone": this.phone,
    };
  }
}
