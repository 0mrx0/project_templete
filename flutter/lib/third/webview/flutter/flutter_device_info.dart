import 'package:device_info/device_info.dart';

///设备信息
class DeviceInfoParams {
  String systemName;
  String systemVersion;
  String model;
  String uuid;
  String screenWidth;
  String screenHeight;

  DeviceInfoParams.fromDevice(AndroidDeviceInfo device) {
    this.model = device.model;
    this.systemName = "Android";
    this.systemVersion = device.version.baseOS;
    this.uuid = device.androidId;
  }

  Map<String, dynamic> toJson() {
    return {
      "systemName": this.systemName,
      "systemVersion": this.systemVersion,
      "model": this.model,
      "uuid": this.uuid,
      "screenWidth": this.screenWidth,
      "screenHeight": this.screenHeight,
    };
  }
}
