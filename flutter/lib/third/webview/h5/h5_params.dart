
class H5Params {
     String action; //代表要执行的方法名
     Object params; //h5给原生的数据
     String jsCallBackId; //回调h5的方法id

     H5Params.fromJson(Map<String, dynamic> json) {
          action = json['action'];
          jsCallBackId = json['jsCallBackId'];
          params = json['params'];
     }

     Map<String, dynamic> toJson() {
    return {
      'action': action,
      'params': params?.toString(),
      'jsCallBackId': jsCallBackId,
    };
  }
}
