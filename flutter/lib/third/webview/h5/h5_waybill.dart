///@date  :2020/11/10/16:37
///desc：h5打印标签时，传递的运单对象
///author：gyy

class H5Waybill{
  String waybillNo;
  int quantity;


  H5Waybill({this.waybillNo, this.quantity});

  factory H5Waybill.fromJson(Map<String, dynamic> json) {
    return H5Waybill(
      waybillNo: json["waybillNo"],
      quantity: int.parse(json["quantity"]),
    );
  }
//

}