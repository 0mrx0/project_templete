import 'h5_waybill.dart';

///h5打印参数
class PrintParams {
  String compCode;
  String waybillNo;
  String forwardName;

  //配载单号
  String stowageNo;

  //派件联打印 运单号集合
  List<String> waybillNos;

  // Object waybillNos;
  //图片的url
  String imageUrl;

  //批量打印运单- 集合
  List<H5Waybill> waybillList;

  // Object waybillList;
  // 1：补印，0：不是补印
  String isReprint;

  PrintParams(
      {this.compCode,
      this.waybillNo,
      this.forwardName,
      this.stowageNo,
      this.waybillNos,
      this.imageUrl,
      this.waybillList,
      this.isReprint});

  factory PrintParams.fromJson(Map<String, dynamic> json) {
    return PrintParams(
      compCode: json["compCode"],
      waybillNo: json["waybillNo"],
      forwardName: json["forwardName"],
      stowageNo: json["stowageNo"],
      waybillNos: json["waybillNos"] == null
          ? null
          : List.of(json["waybillNos"]).map((i) => i).toList(),
      imageUrl: json["imageUrl"],
      waybillList: json["waybillList"] == null
          ? null
          : List.of(json["waybillList"])
              .map((i) => H5Waybill.fromJson(i))
              .toList(),
      isReprint: json["isReprint"],
    );
  }
//

//

}
