import 'dart:convert';

import 'package:device_info/device_info.dart';
import 'package:flutter_ywt/common/menu_click.dart';
import 'package:flutter_ywt/config/device_manager.dart';
import 'package:flutter_ywt/config/router_manager.dart';
import 'package:flutter_ywt/model/login/login_data.dart';
import 'package:flutter_ywt/store/index.dart';
import 'package:flutter_ywt/store/store_login.dart';
import 'package:flutter_ywt/store/store_menu.dart';
import 'package:flutter_ywt/third/urllauncher/url_launcher.dart';
import 'package:flutter_ywt/third/webview/h5/h5_params.dart';
import 'package:flutter_ywt/third/webview/h5/h5_print.dart';
import 'package:flutter_ywt/third/webview/h5/h5_waybill.dart';
import 'package:flutter_ywt/utils/app_util.dart';
import 'package:flutter_ywt/utils/toast_util.dart';

import 'flutter/flutter_contact.dart';
import 'flutter/flutter_device_info.dart';
import 'flutter/flutter_params.dart';


///webview传过来的回调函数
Function(String result) _callJWebView;

///flutter -> h5 ( 回调js的方法)
callJs(FlutterParams flutterParams) {
  String str = JsonEncoder().convert(flutterParams);
  _callJWebView(str);
}

//获取H5Params的参数
dynamic getParamByName(H5Params h5Params, String name) {
  //对象 -> string -> map
  Object params = h5Params.params;
  String str = JsonEncoder().convert(params);
  Map<dynamic, dynamic> map = JsonDecoder().convert(str);
  return map[name];
}

///h5 -> flutter (回调flutter的方法)
callFlutter(H5Params h5Params, Function(String result) callJWebView) {
  _callJWebView = callJWebView;
  String jsCallBackId = h5Params.jsCallBackId;
  String action = h5Params.action;
  Object obj = h5Params.params;
//  toastUtil(action);
  switch (action) {
    case "getUserInfo":
      getUserInfo(
        jsCallBackId,
      );
      break;
    case "getDeviceInfo": //获取设备信息
      getDeviceInfo(jsCallBackId);
      break;
    case "nativeScan": // 扫描
      nativeScan(h5Params);
      break;
    case "nativePrint": // 打印
      nativePrint(h5Params);
      break;
    case "exitApp": // 退出app
      exitApp(h5Params);
      break;
    case "licenseSpot": // 读取身份证
      licenseSpot(h5Params);
      break;
    case "closeWebView": // 关闭webview
      closeWebView(h5Params);
      break;
    case "openPhoneBook": // 打开通讯录，选择联系人
      openPhoneBook(h5Params);
      break;
    case "callPhone": // 打电话
      callPhone(h5Params);
      break;
    case "jumpMoreModules": // 10 更多菜单跳转
      jumpMoreModules(h5Params);
      break;
    case "privilegeCode": // 11 点击菜单跳转
      privilegeCode(h5Params);
      break;
    case "privilegeCodeExtras": // 点击菜单跳转,带参数
      privilegeCodeExtras(h5Params);
      break;
  }
}

void privilegeCodeExtras(H5Params h5Params) {
  String code = getParamByName(h5Params, "privilegeCode");
  String extras = getParamByName(h5Params, "extras").toString();

//  switch (code) {
//    case GN_FZGN010: //智能派送
//      Routers.push("智能派送", 1);
//      return;
//    case GN_CXGN004: //统计
//      String activeNameUrl = ywtUrl + tongjiH5 + "?activeName=" + extras;
//      Routers.push(RouteName.flutter_webview,
//          {INTENT_URL: activeNameUrl});
//      return;
//
//    case GN_SMCZ002: // 签收详情 -需本地请求数据后，才进行跳转
//
//      MenuClick.onMenuClick(Store.value<StoreMenu>().codeMenu("GN_SMCZ002"));
//      return;
//    case GN_CXGN005: //网点钱包
//      var menu = Store.value<StoreMenu>().codeMenu(GN_CXGN005);
//      String url = menu.route + "accountBill/?incomeDirection=" + extras;
//      Routers.push(
//          RouteName.flutter_webview, {INTENT_URL: url, INTENT_TITLE: "网点钱包"});
//      return;
//  }
  //case不到的，根据code跳转
  privilegeCode(h5Params);
}

void privilegeCode(H5Params h5Params) {
  String privilegeCode = getParamByName(h5Params, "privilegeCode");
  var menu = Store.value<StoreMenu>().codeMenu(privilegeCode);
  MenuClick.onMenuClick(menu);
}

void jumpMoreModules(H5Params h5params) {
  var menu = Store.value<StoreMenu>().moreMenu();
  MenuClick.onMenuClick(menu);
}

void callPhone(H5Params h5Params) {
  String phoneNum = getParamByName(h5Params, "phoneNum");
  UrlLauncher.tel(phoneNum);
}

void openPhoneBook(H5Params h5params) async {
//  final ContactPicker _contactPicker = new ContactPicker();
//  Contact contact = await _contactPicker.selectContact();
//
//  FlutterContact flutterContact = FlutterContact();
//  flutterContact.phone = contact.phoneNumber.number;
//  flutterContact.name = contact.fullName;
//
//  print(flutterContact.toJson());
//
//  FlutterParams params = FlutterParams();
//  params.data = flutterContact;
//  params.jsCallBackId = h5params.jsCallBackId;
//  callJs(params);

}

void closeWebView(H5Params h5params) {
  Routers.pop();
}

void licenseSpot(H5Params h5params) {
  toastUtil("读取身份证");
}

void exitApp(H5Params h5params) {
  AppUtil.exitApp();
}

void nativePrint(H5Params h5Params) {
  String printType = getParamByName(h5Params, "printType");
  Object data = getParamByName(h5Params, "data");
  String str = JsonEncoder().convert(data);
  Map<String, dynamic> map = JsonDecoder().convert(str);
  //直接转map
  PrintParams printParams = PrintParams.fromJson(map);

  List<String> waybillNos = printParams.waybillNos; //打印派件联 - 运单号集合
  String stowageNo = printParams.stowageNo; //配载单
  String imageUrl = printParams.imageUrl; //图片url
  List<H5Waybill> waybillH5s = printParams.waybillList; //开单打印, 打印标签 - 单号集合
  String waybillNo = printParams.waybillNo; //寄件联打印 - 运单号
  String isReprint = printParams.isReprint;

  Map<String, String> params = Map();
  params["currentDept"] = "test02";
  params["biz"] = "5";
  params["productCode"] = "CP13";
  params["isApp"] = "1";
  params["businessAttribute"] = "1";

}

getUserInfo(String jsCallBackId) {
  StoreLogin loginStore = Store.value<StoreLogin>();
  LoginData mData = loginStore.mData;
  FlutterParams flutterParams = FlutterParams();
  flutterParams.jsCallBackId = jsCallBackId;
  flutterParams.data = mData;
  callJs(flutterParams);
}

void getDeviceInfo(String jsCallBackId) async {
  AndroidDeviceInfo device = await DeviceMan.getDeviceInfo();
  var deviceInfo = DeviceInfoParams.fromDevice(device);
  FlutterParams flutterParams = FlutterParams();
  flutterParams.jsCallBackId = jsCallBackId;
  flutterParams.data = deviceInfo;
  callJs(flutterParams);
}

void nativeScan(H5Params h5Params) {
  FlutterParams flutterParams = FlutterParams();

  Future res = Routers.push(RouteName.scan_page);
  res.then((msg) => {
        flutterParams.jsCallBackId = h5Params.jsCallBackId,
        flutterParams.data = msg,
        callJs(flutterParams),
      });
}
