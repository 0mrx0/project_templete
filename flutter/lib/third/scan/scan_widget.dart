import 'package:ai_barcode/ai_barcode.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ywt/config/theme/size.dart';
import 'package:permission_handler/permission_handler.dart';

String _label;
Function(String result) _resultCallback;

///扫描的组件
class AppBarcodeScannerWidget extends StatefulWidget {
  ///构造函数传参： 回调函数，标签说明
  AppBarcodeScannerWidget.defaultStyle({Function(String result) resultCallback, String label = '单号',}) {
    _resultCallback = resultCallback ?? (String result) {};
    _label = label;
  }

  @override
  _AppBarcodeState createState() => _AppBarcodeState();
}

class _AppBarcodeState extends State<AppBarcodeScannerWidget> {
  @override
  Widget build(BuildContext context) {
    return _BarcodePermissionWidget();
  }
}

class _BarcodePermissionWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _BarcodePermissionWidgetState();
  }
}

class _BarcodePermissionWidgetState extends State<_BarcodePermissionWidget> {
  bool _isGranted = false;

  bool _useCameraScan = true;

  String _inputValue = "";

  @override
  void initState() {
    super.initState();

    _requestPermission();
  }

  void _requestPermission() async {
    if (await Permission.camera.request().isGranted) {
      setState(() {
        _isGranted = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: _isGranted
              ? _useCameraScan
              ? _BarcodeScannerWidget()
              : _BarcodeInputWidget.defaultStyle(
            changed: (String value) {
              _inputValue = value;
            },
          )
              : Center(
            child: OutlineButton(
              onPressed: () {
                _requestPermission();
              },
              child: Text("请求权限"),
            ),
          ),
        ),

        Container(
          height: size50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              RaisedButton(onPressed: (){},
              child: Text("扫描单号"),
              ),
              RaisedButton(onPressed: (){},
                child: Text("扫码登录"),
              )
            ],
          ),
        ),

//        _useCameraScan
//            ? OutlineButton(
//          onPressed: () {
//            setState(() {
//              _useCameraScan = false;
//            });
//          },
//          child: Text("手动输入$_label"),
//        )
//            : Row(
//          children: [
//            OutlineButton(
//              onPressed: () {
//                setState(() {
//                  _useCameraScan = true;
//                });
//              },
//              child: Text("扫描$_label"),
//            ),
//            OutlineButton(
//              onPressed: () {
//                _resultCallback(_inputValue);
//              },
//              child: Text("确定"),
//            ),
//          ],
//        ),


      ],
    );
  }
}

class _BarcodeInputWidget extends StatefulWidget {
  ValueChanged<String> _changed;

  _BarcodeInputWidget.defaultStyle({
    ValueChanged<String> changed,
  }) {
    _changed = changed;
  }

  @override
  State<StatefulWidget> createState() {
    return _BarcodeInputState();
  }
}

class _BarcodeInputState extends State<_BarcodeInputWidget> {
  final _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _controller.addListener(() {
      final text = _controller.text.toLowerCase();
      _controller.value = _controller.value.copyWith(
        text: text,
        selection:
        TextSelection(baseOffset: text.length, extentOffset: text.length),
        composing: TextRange.empty,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(padding: EdgeInsets.all(8)),
        Row(
          children: <Widget>[
            Padding(padding: EdgeInsets.all(8)),
            Text(
              "$_label：",
            ),
            Expanded(
              child: TextFormField(
                controller: _controller,
                onChanged: widget._changed,
                decoration: InputDecoration(border: OutlineInputBorder()),
              ),
            ),
            Padding(padding: EdgeInsets.all(8)),
          ],
        ),
        Padding(padding: EdgeInsets.all(8)),
      ],
    );
  }
}

///ScannerWidget
class _BarcodeScannerWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AppBarcodeScannerWidgetState();
  }
}

class _AppBarcodeScannerWidgetState extends State<_BarcodeScannerWidget> {
  ScannerController _scannerController;

  var start = 0;
  @override
  void initState() {
    super.initState();

    _scannerController = ScannerController(scannerResult: (result) {
      //回调扫描结果
      _resultCallback(result);
      //延迟一秒，再次扫描
//      Future.delayed(Duration(seconds: 1), () {
//        _scannerController.startCameraPreview();
//      });

    },
        scannerViewCreated: () {
      TargetPlatform platform = Theme.of(context).platform;
      if (TargetPlatform.iOS == platform) {
        Future.delayed(Duration(seconds: 2), () {
          _scannerController.startCamera();
          _scannerController.startCameraPreview();
        });
      } else {
        _scannerController.startCamera();
        _scannerController.startCameraPreview();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();

    _scannerController.stopCameraPreview();
    _scannerController.stopCamera();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: _getScanWidgetByPlatform(),
        )
      ],
    );
  }

  Widget _getScanWidgetByPlatform() {
    return PlatformAiBarcodeScannerWidget(
      platformScannerController: _scannerController,
    );
  }
}