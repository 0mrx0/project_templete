import 'package:flutter/material.dart';
import 'package:flutter_ywt/config/router_manager.dart';
import 'package:flutter_ywt/ui/weight/tool_bar.dart';

import 'scan_widget.dart';

///扫描页

class FullScreenScannerPage extends StatefulWidget {
  @override
  _FullScreenScannerPageState createState() => _FullScreenScannerPageState();
}

class _FullScreenScannerPageState extends State<FullScreenScannerPage> {
  String _code = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ToolBar(
        title: "扫描 $_code",
      ),
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: AppBarcodeScannerWidget.defaultStyle(
                resultCallback: (String code) {
                  setState(() {
                    _code = code;
                    Routers.pop(_code);
                  });
                },
              ),
            ),
          ],
        ),
      ),

    );
  }
}