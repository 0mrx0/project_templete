import 'view_state_base_model.dart';
///@date  :2020/11/10/16:37
///desc： 单实体
///author：gyy

/// 返回数据是 单实体
abstract class ViewStateBeanModel<T> extends ViewStateBaseModel {
  // 页面数据
  T dataBean;

  /// 第一次进入页面loading skeleton
  initData() async {
    setBusy();
    await refresh(init: true);
  }

  // 下拉刷新
  refresh({bool init = false}) async {
    try {
      T data = await loadData();
      if (data == null) {
        dataBean = null;
        setEmpty();
      } else {
        onCompleted(dataBean);
        dataBean = data;
        setIdle();
      }
    } catch (e, s) {
      if (init)
        dataBean = null;
      setError(e, s);
    }
  }

  // 加载数据
  Future<T> loadData();
  //设置参数
  setParameters(Map<String, dynamic> map) {
    this.queryParameters = map;
  }
  onCompleted(T d) {}
}

