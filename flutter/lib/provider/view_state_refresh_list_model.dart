import 'package:flutter/cupertino.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'view_state_list_model.dart';

/// 返回数据是列表，并且需要配合上拉 下拉
abstract class ViewStateRefreshListModel<T> extends ViewStateListModel<T> {
  /// 分页第一页页码
  static const int pageNumFirst = 0;
  /// 分页条目数量
  static const int pageSize = 10;
  /// 当前页码
  int _currentPageNum = pageNumFirst;

  RefreshController _refreshController = RefreshController(initialRefresh: false);
  RefreshController get refreshController => _refreshController;


  /// 下拉刷新
  /// [init] 是否是第一次加载
  Future<List<T>> refresh({bool init = false}) async {
    try {
      //更新参数
      _currentPageNum = pageNumFirst;
      queryParameters['offset'] = pageNumFirst;

      var data = await loadData();
      if (data.isEmpty) {
        refreshController.refreshCompleted(resetFooterState: true);
        list.clear();
        setEmpty();
      } else {
        onCompleted(data);
        list.clear();
        list.addAll(data);
        refreshController.refreshCompleted();
        // 小于分页的数量,禁止上拉加载更多
        if (data.length < pageSize) {
          refreshController.loadNoData();
        } else {
          //防止上次上拉加载更多失败,需要重置状态
          refreshController.loadComplete();
        }
        //notifyListeners
        setIdle();
      }
      return data;
    } catch (e, s) {
      /// 页面已经加载了数据,如果刷新报错,不应该直接跳转错误页面
      /// 而是显示之前的页面数据.给出错误提示
      if (init) list.clear();
      refreshController.refreshFailed();
      setError(e, s);
      return null;
    }
  }

  /// 上拉加载更多
  Future<List<T>> loadMore() async {
    try {
      var data = await loadData();
      if (data.isEmpty) {
        _currentPageNum = (--_currentPageNum) * pageSize;
        refreshController.loadNoData();
      } else {
        onCompleted(data);
        list.addAll(data);
        if (data.length < pageSize) {
          refreshController.loadNoData();
        } else {
          refreshController.loadComplete();
        }
        notifyListeners();
      }
      return data;
    } catch (e, s) {
      _currentPageNum = (--_currentPageNum) * pageSize;
      refreshController.loadFailed();
      debugPrint('error--->\n' + e.toString());
      debugPrint('statck--->\n' + s.toString());
      return null;
    }
  }

  @override
  onCompleted(List<T> data) {
    //更新参数
    queryParameters['offset'] = (++_currentPageNum) * pageSize;
    return super.onCompleted(data);
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }
}
