import 'package:flutter_ywt/http/repository_home.dart';
import 'package:flutter_ywt/provider/view_state_refresh_list_model.dart';

///@date  :2020/11/10/16:37
///desc： 发现页model
///author：gyy

class NoticeModel extends ViewStateRefreshListModel{
  @override
  Future<List> loadData() async{
    return await HomeRepository.publishedPost(queryParameters);
  }

}