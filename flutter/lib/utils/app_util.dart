import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ywt/ui/weight/dialog_loading.dart';
import 'package:flutter_ywt/ui/weight/dialog_progress.dart';

class AppUtil {

  static Future<void> exitApp() async {
    await SystemChannels.platform.invokeMethod('SystemNavigator.pop');
  }

  static showLoading(BuildContext context, String title) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return new LoadingDialog(
            text: title,
          );
        });
  }
    static showProgress(BuildContext context, String title, double value, {bool canBack = false}) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return new ProgressDialog(
            title, value: value, canBack : canBack
          );
        });
  }


}
