import 'package:sprintf/sprintf.dart';

const stowage_id = "6474095688572547072";
const dispatch_id = "6564741851293696000";

const stub_sends_id1 = "6513321509426036736";
const stub_sends_id2 = "6513321422427869184";

///生成运单子单号集合
childGenerate(String waybill, int num) {
  List<String> childList = List();
  for(int i = 1; i <= num; i++ ) {
    childList.add(getChildren(waybill, i));
  }
  return childList;
}


 String getChildren(String mWayBillNo, int num) {
  if (num < 999) {
    return mWayBillNo + sprintf("%03d", [num]);
  }
  return "错误子单号";
}