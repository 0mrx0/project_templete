import 'package:flutter/material.dart';
import 'package:oktoast/oktoast.dart';

toastUtil(String message) {
  showToast(message, textPadding: EdgeInsets.fromLTRB(16, 8, 16, 8));
}