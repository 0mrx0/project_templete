import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'store_login.dart';
import 'store_menu.dart';

///全局的状态管理
class Store {
  static BuildContext _context;

  static init(child) {
    return MultiProvider(
      child: child,
      providers: [
        //需要全局管理的状态，加在这里

        //登录信息
        ChangeNotifierProvider.value(value: StoreLogin()),
        //菜单信息
        ChangeNotifierProvider.value(value: StoreMenu()),
      ],
    );
  }

  static setContext(BuildContext context) {
    Store._context ??= context;
    return context;
  }

  static T value<T>({BuildContext context, bool listen = false}) {
    context ??= Store._context;
    return Provider.of<T>(context, listen: listen);
  }


}

