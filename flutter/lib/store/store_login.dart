import 'package:flutter/foundation.dart';
import 'package:flutter_ywt/model/login/login_data.dart';

///描述：登陆信息
///author：gyy
class StoreLogin with ChangeNotifier{
  LoginData mData;
  setData(LoginData mData) {
    this.mData = mData;
    notifyListeners();
  }

}