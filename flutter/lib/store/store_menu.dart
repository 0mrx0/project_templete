import 'package:flutter/foundation.dart';
import 'package:flutter_ywt/constant/menu_data.dart';
import 'package:flutter_ywt/model/common/permission.dart';

///描述：菜单信息
///author：gyy
class StoreMenu with ChangeNotifier{

  //权限集合
  List<PermissionMenu> mData;
  setData(List<PermissionMenu> mData) {
    this.mData = mData;
    notifyListeners();
  }

  //根据 name字段，过滤菜单
  PermissionMenu codeMenu(String name) {
    var menu = mData.firstWhere((element) => element.name == name);
    return menu;
  }

  //更多菜单
  PermissionMenu moreMenu() {
    return PermissionMenu(name: "更多", type: 2);
  }

  //根据 type字段，过滤菜单
  List<PermissionMenu> levelMenu(int level) {
    if(mData == null) {
      return [];
    }
    List<PermissionMenu> levelList = mData.where((item) => item.type == level).toList();
    return levelList;
  }


  //中间区域菜单
  List<PermissionMenu> homeContentMenu([List<PermissionMenu> list]) {
    if(mData == null) {
      return [];
    }
    if(list == null) {
      list = mData;
    }
//    List<PermissionMenu> levelList2  = levelMenu(2);
//    List<PermissionMenu> homeHeaderList = homeHeaderMenu();
//    levelList2.removeWhere((element) => homeHeaderList.contains(element));

    List<PermissionMenu> levelList = list.where((item) => contentList.contains(item.name)).toList();
    //添加 更多菜单
    levelList.add(moreMenu());
    return levelList;
  }

  //顶部3个菜单
  List<PermissionMenu> homeHeaderMenu([List<PermissionMenu> list]) {
    if(mData == null) {
      return [];
    }
    if(list == null) {
      list = mData;
    }
    List<PermissionMenu> levelList = list.where((item) => headerList.contains(item.name)).toList();
    return levelList;
  }

}