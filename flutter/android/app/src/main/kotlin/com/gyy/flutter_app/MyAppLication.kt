package com.gyy.flutter_ywt

import android.content.Context
import androidx.multidex.MultiDex
import com.baidu.flutter_bmfbase.BmfMapApplication
///初始化百度地图
class MyAppLication : BmfMapApplication() {
    override fun onCreate() {
        super.onCreate()
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}