package com.boot.security.server.dto;

import java.io.Serializable;

/**
 * 返回给前端的 参数
 */
public class ResponseInfo implements Serializable {

	private static final long serialVersionUID = -4417715614021482064L;

	private int code;
	private String message;
	private Object data;
	//h5分页参数
	private Integer recordsTotal;
	//h5分页参数
	private Integer recordsFiltered;

	public ResponseInfo(int code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public ResponseInfo(int code, String message, Object data) {
		super();
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public ResponseInfo(int code, Object data, int recordsTotal, int recordsFiltered) {
		super();
		this.recordsTotal = recordsTotal;
		this.recordsFiltered = recordsFiltered;
		this.code = code;
		this.data = data;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(Integer recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public Integer getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(Integer recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}
}
