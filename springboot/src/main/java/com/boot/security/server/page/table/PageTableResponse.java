package com.boot.security.server.page.table;

import com.boot.security.server.dto.ResponseInfo;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.util.List;

/**
 * 分页查询返回
 * 
 * @author 2020.12.17.commit
 *
 */
public class PageTableResponse extends ResponseInfo{

	public PageTableResponse(Integer recordsTotal, Integer recordsFiltered, Object data) {
		super(HttpStatus.OK.value(), data, recordsTotal, recordsFiltered);
	}

}