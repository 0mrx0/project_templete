package com.boot.security.server.utils;

import com.alibaba.fastjson.JSONObject;
import com.boot.security.server.dto.ResponseInfo;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ResponseUtil {

	public static void responseJson(HttpServletResponse response, int status, Object data) {
		try {
			ResponseInfo responseInfo = new ResponseInfo(status, null, data);
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Access-Control-Allow-Methods", "*");
			response.setContentType("application/json;charset=UTF-8");
			response.setStatus(status);
			response.getWriter().write(JSONObject.toJSONString(responseInfo));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//RestController中，返回给前端的数据
	public static ResponseInfo responseOk(Object data) {
		return  new ResponseInfo(HttpStatus.OK.value(), null, data);
	}

	//RestController中，返回给前端的数据
	public static ResponseInfo responseError(Object data) {
		return  new ResponseInfo(HttpStatus.INTERNAL_SERVER_ERROR.value(), data.toString(), null);
	}

}
