export default {
  path: 'notFound',
  name: 'notFound',
  meta: {
    title: '页面未找到',
    notLogin: true
  },
  component: () => import(/* webpackChunkName: "notFound" */ './src/index.vue')
}
