// 路由
export default {
  path: "index",
  name: "index",
  meta: {
    title: "壹网通"
  },
  component: () => import(/* webpackChunkName: "index" */ "./src/index.vue")
};
