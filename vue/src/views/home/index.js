// 路由
export default {
  path: "home",
  name: "home",
  meta: {
    title: "壹网通"
  },
  component: () => import(/* webpackChunkName: "home" */ "./src/index.vue")
};
