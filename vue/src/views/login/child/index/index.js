import index from "./src/index.vue";

export default {
  name: "login/index",
  path: "index",
  meta: {
    title: "登录"
  },
  component: index
};
