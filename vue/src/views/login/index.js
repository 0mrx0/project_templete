import index from "./child/index/index";
import password from "./child/password/index";
export default {
  path: "login",
  name: "login",
  redirect: {
    name: "login/password"
  },
  meta: {
    title: "登录"
  },
  component: () => import(/* webpackChunkName: "login" */ "./src/index.vue"),
  children: [index, password]
};
