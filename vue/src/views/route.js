import App from "../App.vue";
// 壹网通官网
import index from "./index";
import home from "./home";
import login from "./login";
// 404页面
import notFound from "./notFound";
const routes = [
  {
    path: "/",
    component: App,
    children: [
      // 壹网通官网
      index,
      home,
      login,
      notFound,
      {
        path: "*",
        redirect: {
          name: "notFound"
        }
      }
    ]
  }
];

export default routes;
