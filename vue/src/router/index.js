import Vue from "vue";
import VueRouter from "vue-router";
import routes from "../views/route";

Vue.use(VueRouter);
console.log(routes)
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
