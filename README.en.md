# project_templete

#### Description
项目开发模板（java基础模板，flutter基础模板，vue基础模板）

#### 目录说明

1.  springboot目录：是java项目基础模板 （细节说明参考对应目录下的readme文档）
2.  flutter目录：是flutter项目基础模板 （细节说明参考对应目录下的readme文档）
3.  vue目录：是vue项目基础模板 （细节说明参考对应目录下的readme文档）

#### Instructions

1.  spring-boot是一个完整的项目，提供接口 和后端管理界面
2.  flutter是一个完整的项目，数据来自spring-boot项目提供的接口
3.  vue是一个完整的项目，数据来自spring-boot项目提供的接口
4.  三个子项目构成一个完整的项目，包含前后端，移动端，是学习全栈开发的不错的参考项目

#### Contribution

1.  后台演示地址：http://47.101.170.41:8090/
2.  用户名和密码都是 user
3.  flutter，vue项目可直接运行，访问的是我的服务器，也可替换为你自己的地址
4.  视频讲解地址：https://space.bilibili.com/392994784
